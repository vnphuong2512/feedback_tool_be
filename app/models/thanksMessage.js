class ThanksMessage {
  constructor(customerId, iconColorPicker, iconName, messageTitle, messageContent,id) {
    this.customerId = customerId;
    this.iconColorPicker = iconColorPicker;
    this.iconName = iconName;
    this.messageTitle = messageTitle;
    this.messageContent = messageContent;
    this.id = id
  }
}

module.exports = ThanksMessage;
