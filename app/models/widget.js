class Widget {
  constructor(customerId, buttonBgColor, buttonPosition, buttonStyle, buttonText, iconName, textColor, logo, id) {
    this.customerId = customerId;
    this.buttonBgColor = buttonBgColor;
    this.buttonPosition = buttonPosition;
    this.buttonStyle = buttonStyle;
    this.buttonText = buttonText;
    this.iconName = iconName;
    this.textColor = textColor;
    this.logo = logo;
    this.id = id
  }
}

module.exports = Widget;
