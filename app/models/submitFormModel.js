class SubmitForm {
  constructor(
    aboutChoose,
    address,
    comment,
    country,
    deviceChoose,
    email,
    fileUpload,
    fullName,
    gender,
    phone,
    rate,
    role,
    screenImg
  ) {
    this.aboutChoose = aboutChoose;
    this.address = address;
    this.comment = comment;
    this.country = country;
    this.deviceChoose = deviceChoose;
    this.email = email;
    this.fileUpload = fileUpload;
    this.fullName = fullName;
    this.gender = gender;
    this.phone = phone;
    this.rate = rate;
    this.role = role;
    this.screenImg = screenImg;
  }
}

module.exports = SubmitForm;
