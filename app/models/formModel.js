class Form {
  constructor(data, formName,iconName, title, id) {
    this.data = data;
    this.formName = formName;
    this.iconName = iconName;
    this.title = title;
    this.id = id;
  }
}

module.exports = Form;
