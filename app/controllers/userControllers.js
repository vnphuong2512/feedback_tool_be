"use strict";

const firebase = require("../../db");

const User = require("../models/userModel");
const firestore = firebase.firestore();
const uniqid = require('uniqid')
const bcrypt = require('bcrypt')

// check if user is exist on database
const checkUserExist = async (email) => {
  try {
    const querySnapshot = await firestore
      .collection("FEEDBACK")
      .doc("USERS")
      .collection("USERS")
      .where("email", "==", email)
      .get();

    if (querySnapshot.empty) {
      return null;
    }

    // Get the first matching document's data
    const userSnapshot = querySnapshot.docs[0];
    const userData = userSnapshot.data();

    return userData;
  } catch (error) {
    throw error;
  }
};
const checkUserExistByUid = async (uid) => {
  try{
    const querySnapshot = await firestore
      .collection("FEEDBACK")
      .doc("USERS")
      .collection("USERS")
      .doc(uid)
      .get()
    if (querySnapshot.empty){
        return null
      }
    const userData = querySnapshot.data();

    return userData;
  }
  catch(error){
    throw error
  }
}

//Create User
const addUser = async (req, res) => {
  try {
    let { username, email, password } = req.body;

    if (!username || !email || !password) {
      return res.status(500).json({ message: "Please provide all the data require" });
    }
    // check if user is exist first
    const isValidUser = await checkUserExist(email)
    if (isValidUser) {
      return res.status(409).json({ message: 'User already existed' })
    }
    // case: no user => create new one
    const hashPass = await bcrypt.hash(password, 10)
    const user = {username, email, password: hashPass}

    // await firestore.collection("USERS").doc().set(data);
    const userRef = await firestore
      .collection("FEEDBACK")
      .doc("USERS")
      .collection("USERS")
      .doc();

    await userRef.set(Object.assign({}, user));

    // Get the newly created user data
    const newUserSnapshot = await userRef.get();
    const newUser = {id: newUserSnapshot.id,...newUserSnapshot.data()};
    delete newUser.password
    res.status(200).json(newUser);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

//Get All User
const getAllUsers = async (req, res) => {
  try {
    // const users = await firestore.collection("USERS");
    const users = await firestore
      .collection("FEEDBACK")
      .doc("USERS")
      .collection("USERS");
    const data = await users.get();
    const usersArr = [];
    if (data.empty) {
      res.status(404).send("No User record found");
    } else {
      data.forEach((doc) => {
        const user = new User(
          doc.id,
          doc.data().username,
          doc.data().email,
        );
        usersArr.push(user);
      });
      res.send(usersArr);
    }
  } catch (err) {
    res.status(400).send(err.message);
  }
};

//getUserbyId
const getUserById = async (req, res) => {
  try {
    const id = req.params.id;
    // const user = await firestore.collection("USERS").doc(id);
    const user = await firestore
      .collection("FEEDBACK")
      .doc("USERS")
      .collection("USERS")
      .doc(id);
    const data = await user.get();
    if (!data.exists) {
      res.status(404).send("User with the given ID is not found");
    } else {
      res.send(data.data());
    }
  } catch (err) {
    res.status(400).send(err.message);
  }
};

module.exports = {
  addUser,
  getAllUsers,
  getUserById,
  checkUserExist,
  checkUserExistByUid
};
