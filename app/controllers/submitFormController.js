"use strict";

const firebase = require("../../db");
const SubmitForm = require("../models/submitFormModel");
const firestore = firebase.firestore();
require("dotenv").config();

const output = process.env.HOST_URL
const now = new Date()
const currentDay = now.getFullYear() + 'Y' + now.getMonth() + 'M' + now.getDate() + 'D' + now.getHours() + 'H' + now.getMinutes() + 'M' + now.getSeconds() + 'S' + now.getMilliseconds() + 'MS'

//Create Form
const createSubmitForm = async (req, res) => {
  try {
    let pathFileScreenshotConvert = [];
    let pathFileUploadConvert;
    if (req.files !== undefined && req.files !== null && req.files.screenshot !== null && req.files.attachment !== null) {
      let screenshot = req?.files?.screenshot;
      console.log('screenshot: ', screenshot);
      let attachment = req?.files?.attachment;

      if (attachment !== undefined) {
        const fileUpload = attachment?.name.split('.')[0].replaceAll(' ', '') + '_' + currentDay + '.png';
        attachment.mv('./submission/upload/' + fileUpload);
        pathFileUploadConvert = output + '/'+ 'upload/' + fileUpload;
      }

      if (screenshot !== undefined && screenshot !== null) {
        const fileScreenshot = Array.isArray(screenshot) ?
          screenshot?.map((item) => item.name.split('.')[0].replaceAll(' ', '') + '_' + currentDay + '.png')
          : screenshot?.name.split('.')[0].replaceAll(' ', '') + '_' + currentDay + '.png';

        if (Array.isArray(fileScreenshot)) {
          for (let i = 0; i < fileScreenshot?.length; i++) {
            const screenshotPath = './submission/screenshot/' + fileScreenshot[i];
   
            screenshot[i].mv(screenshotPath);
            const localLink = output + '/' + 'screenshot/' + fileScreenshot[i];
            pathFileScreenshotConvert.push(localLink);
          }
        } else {
          const screenshotPath = './submission/screenshot/' + fileScreenshot;
          screenshot.mv(screenshotPath);
          const localLink = output + '/' + 'screenshot/' + fileScreenshot;
          pathFileScreenshotConvert.push(localLink);
        }
      }
    }
 
    let submitData = {
      screenshot: pathFileScreenshotConvert?.map(item => ({ path: item })),
      attachment: pathFileUploadConvert,
      ...req.body,
    };

    console.log('submitData: ', submitData);

    const submitRef = await firestore
      .collection('FEEDBACK')
      .doc('SUBMIT')
      .collection('Demo Form Submit')
      .doc();

    await submitRef.set(submitData);

    res.send(submitRef.id);

  } catch (err) {
    console.log('err: ', err);
    res.status(500).send(err.message);
  }
};

const getAllSubmitForm = async (req, res) => {
  try {
    const submitForms = await firestore
      .collection("FEEDBACK")
      .doc("SUBMIT")
      .collection("Demo Form Submit");
    const data = await submitForms.get();
    const submitFormArr = [];
    if (data.empty) {
      res.status(404).send("No form record found");
    } else {
      data.forEach((doc) => {
        const form = { id: doc.id, ...doc.data }
        submitFormArr.push(form);
      });
      res.send(submitFormArr);
    }
  } catch (err) {
    res.status(400).send(err.message);
  }
};

const getSubmitFormById = async (req, res) => {
  try {
    const id = req.params.id;
    const submitForms = await firestore
      .collection("FEEDBACK")
      .doc("SUBMIT")
      .collection("Demo Form Submit")
      .doc(id);
    const data = await submitForms.get();
    if (!data.exists) {
      res.status(404).send("Form with the given ID is not found");
    } else {
      res.status(200).json({ id: data.id, ...data.data() });
    }
  } catch (err) {
    res.status(400).send(err.message);
  }
};

module.exports = {
  createSubmitForm,
  getAllSubmitForm,
  getSubmitFormById,
};
