"use strict";

const firebase = require("../../db");
const Form = require("../models/formModel");
const firestore = firebase.firestore();

const { checkUserExist, checkUserExistByUid, addUser } = require('./userControllers');

//Create Form
const createFrom = async (req, res) => {
  try {
    let { formData, uid =  '' } = req.body;
    let newFormId = [];
    if(!formData || !Array.isArray(formData)){
      return res.status(400).json({message: 'formData must be array'})
    }
    if (!uid?.trim().length) {
      return res.status(400).json({ message: 'Uid is required' })
    }
    for(let i = 0; i < formData.length; i++){
      const { formName, data, iconName, title } = formData[i];
      if (!data || !formName || !iconName || !title) {
        return res.status(500).send({ message: `Invalid form data at position ${i + 1}` });
      }
      const newFormRef = await firestore
        .collection("FEEDBACK")
        .doc("FORM")
        .collection("DEMO FORM")
        .doc()

      await newFormRef.set({ uid, formName, data, iconName, title });
      // can return to client real form => uncomment two line below
    
       newFormId.push(newFormRef.id)
    
    }
  
    res.status(201).json(newFormId);
  } catch (err) {
    res.status(500).send(err.message);
  }
};

//Get All form
const getAllForm = async (req, res) => {
  try {
    // const forms = await firestore.collection("forms");
    const forms = await firestore
      .collection("FEEDBACK")
      .doc("FORM")
      .collection("DEMO FORM");
    const data = await forms.get();
    const formArr = [];
    if (data.empty) {
      res.status(404).send("No form record found");
    } else {
      data.forEach((doc) => {
        const form = new Form(doc.data().data, doc.data().formName, doc.data().iconName, doc.data().title,  doc.id);
        formArr.push(form);
      });
      res.send(formArr);
    }
  } catch (err) {
    res.status(400).send(err.message);
  }
};

// Get From by one or some id
const getFormById = async (req, res) => {
  try {
    const ids = req.params.id;
    const idFormatted = ids?.split('_')
    const formPromises = idFormatted.map(async (id) => {
      const form = await firestore
        .collection("FEEDBACK")
        .doc("FORM")
        .collection("DEMO FORM")
        .doc(id)
        .get();

        console.log('form: ', form.id);
      if (!form.exists) {
        return null; // Return null if form is not found
      } else {
        return { id: form.id, ...form.data() }; // Spread form data directly into the object
      }
    });

    const formData = await Promise.all(formPromises);
    const filteredFormData = formData.filter((data) => data); // Filter out null or undefined values

    res.status(200).json({ formData: filteredFormData });
  } catch (err) {
    res.status(400).send(err.message);
  }
};

module.exports = {
  createFrom,
  getAllForm,
  getFormById,
};
