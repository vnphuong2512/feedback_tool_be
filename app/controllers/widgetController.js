"use strict";

const firebase = require("../../db");
const Widget = require("../models/widget");
const firestore = firebase.firestore();
const uniqid = require('uniqid')
const path = require('path')
const fs = require("fs");
const util = require('util');
require("dotenv").config();

const output = process.env.HOST_URL
const now = new Date()
const currentDay = now.getFullYear() + 'Y' + now.getMonth() + 'M' + now.getDate() + 'D' + now.getHours() + 'H' + now.getMinutes() + 'M' + now.getSeconds() + 'S' + now.getMilliseconds() + 'MS'

//Create Form
const createNewWidget = async (req, res) => {
  try {
    const requiredFields = ['customerId', 'buttonBgColor', 'buttonPosition', 'buttonStyle', 'buttonText', 'iconName', 'textColor'];
    for (const field of requiredFields) {
      if (!req.body[field]) {
        return res.status(500).json({ message: `${field} is required` });
      }
    }
    let { customerId, buttonBgColor, buttonPosition, buttonStyle, buttonText, iconName, textColor } = req.body;
    console.log('req.body: ', req.body);
    let pathFileImageConvert;
    if (req.files) {
      let { file } = req.files;
      console.log('file: ', file);

      const fileName = file.name.split('.')[0].replaceAll(' ', '') + '_' + currentDay + '.png'
      file.mv('./uploads/' + fileName)

      pathFileImageConvert = output + '/'+ fileName
      console.log('pathFileImageConvert: ', pathFileImageConvert);
    }
    
    const newWidgetRef = await firestore
      .collection("FEEDBACK")
      .doc("WIDGET")
      .collection("WIDGET")
      .doc();

    await newWidgetRef.set({
      customerId,
      buttonBgColor,
      buttonPosition,
      buttonStyle,
      buttonText,
      iconName,
      textColor,
      logo: pathFileImageConvert || null
    });

    const newWidgetId = newWidgetRef.id;

    
    res.status(201).json({id: newWidgetId});
  } catch (err) {
    res.status(500).send(err.message);
  }
};

//Get All form
const getAllWidget = async (req, res) => {
  try {
    // const forms = await firestore.collection("forms");
    const widgets = await firestore
      .collection("FEEDBACK")
      .doc("WIDGET")
      .collection("WIDGET")
    const data = await widgets.get();
    const widgetArray = [];
    if (data.empty) {
      res.status(404).send("No form record found");
    } else {
      data.forEach((doc) => {
        const {customerId, buttonBgColor, buttonPosition, buttonStyle, buttonText, iconName, textColor, logo} = doc.data()
        const form = new Widget(customerId, buttonBgColor, buttonPosition, buttonStyle, buttonText, iconName,textColor, logo, doc.id);
        widgetArray.push(form);
      });
      res.send(widgetArray);
    }
  } catch (err) {
    res.status(400).send(err.message);
  }
};

// Get From by id
const getWidgetById = async (req, res) => {
  try {
    const id = req.params.id;
    const widget = await firestore
      .collection("FEEDBACK")
      .doc("WIDGET")
      .collection("WIDGET")
      .doc(id);
    const data = await widget.get();
    if (!data.exists) {
      res.status(404).json({message: "Widget with the given ID is not found"});
    } else {
      res.send(data.data());
    }
  } catch (err) {
    res.status(400).send(err.message);
  }
};

module.exports = {
  createNewWidget,
  getAllWidget,
  getWidgetById,
};
