const path = require('path');
require("dotenv").config();

const output = process.env.HOST_URL

const renderUI = async (req, res) => {
    try {
    const {widget, form, thank} = req.query
    console.log('thanks: ', thank);
    console.log('id: ', widget);
    console.log('formId: ', form);
    // res.sendFile(filePath);
    res.setHeader('content-type', 'text/javascript');
    res.render('feedback/form', {
        formId: form,
        widgetId: widget,
        thanksId: thank,
        url: output,
        layout: false,
    })
    }
    catch(error){
        res.status(500).json({error: error.message})
    }
}
const renderScriptFile = async (req, res) => {
    try {
        const { widget, form, thank } = req.query
        console.log('thanks: ', thank);
        console.log('id: ', widget);
        console.log('formId: ', form);
        // res.sendFile(filePath);

        res.sendFile(path.resolve(__dirname, '..', 'views', 'script.html'))
    }
    catch (error) {
        res.status(500).json({ error: error.message })
    }
}


module.exports = { renderUI, renderScriptFile }