"use strict";

const firebase = require("../../db");
const Thanks = require("../models/thanksMessage");
const firestore = firebase.firestore();

//Create Form
const createThanksContent = async (req, res) => {
  try {
    const requiredFields = ['customerId', 'iconColorPicker', 'iconName', 'messageTitle', 'messageContent'];
    for (const field of requiredFields) {
      if (!req.body[field]) {
        return res.status(500).json({ message: `${field} is required` });
      }
    }
    let { customerId, iconColorPicker, iconName, messageTitle, messageContent } = req.body;

    const thanksData = await firestore
      .collection("FEEDBACK")
      .doc("THANKSMSG")
      .collection("THANKS_SAMPLE")
      .doc()
      
    await thanksData.set({ customerId, iconColorPicker, iconName, messageTitle, messageContent });
    
    res.status(201).json({id: thanksData.id});
  } catch (err) {
    res.status(500).send(err.message);
  }
};

//Get All form
const getAllThankData = async (req, res) => {
  try {
    // const forms = await firestore.collection("forms");
    const thanksData = await firestore
      .collection("FEEDBACK")
      .doc("THANKSMSG")
      .collection("THANKS_SAMPLE")
    const data = await thanksData.get();
    const thanksArray = [];
    if (data.empty) {
      res.status(404).json({ message: "No record found" });
    } else {
      data.forEach((doc) => {
        const {customerId, iconColorPicker, iconName, messageTitle, messageContent} = doc.data()
        const thank = new Thanks(customerId, iconColorPicker, iconName, messageTitle, messageContent, doc.id);
        thanksArray.push(thank);
      });
      res.send(thanksArray);
    }
  } catch (err) {
    res.status(400).send(err.message);
  }
};

// Get From by id
const getThanksDataById = async (req, res) => {
  try {
    const id = req.params.id;
    const form = await firestore
      .collection("FEEDBACK")
      .doc("THANKSMSG")
      .collection("THANKS_SAMPLE")
      .doc(id);
    const data = await form.get();
    if (!data.exists) {
      res.status(404).json({ message: "No record found" });
    } else {
      res.send(data.data());
    }
  } catch (err) {
    res.status(400).send(err.message);
  }
};

module.exports = {
  createThanksContent,
  getAllThankData,
  getThanksDataById,
};
