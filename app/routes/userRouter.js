const express = require("express");
const usercontrollers = require("../controllers/userControllers");

const router = express.Router();

// router.post("/users", addUser);
// router.get("/users", getAllUsers);
// router.get("/users/:id", getUserById);

router.post("/", usercontrollers.addUser);
router.get("/", usercontrollers.getAllUsers);
router.get("/:id", usercontrollers.getUserById);

module.exports = router