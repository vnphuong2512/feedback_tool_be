const express = require("express");
const widgetController = require("../controllers/widgetController");

const router = express.Router();

// router.post("/users", addUser);
// router.get("/users", getAllUsers);
// router.get("/users/:id", getUserById);

router.post("/", widgetController.createNewWidget);
router.get("/", widgetController.getAllWidget);
router.get("/:id", widgetController.getWidgetById);

module.exports = router
