const express = require("express");
const formController = require("../controllers/formController");

const router = express.Router();

// router.post("/users", addUser);
// router.get("/users", getAllUsers);
// router.get("/users/:id", getUserById);

router.post("/", formController.createFrom);
router.get("/", formController.getAllForm);
router.get("/:id", formController.getFormById);

module.exports = router
