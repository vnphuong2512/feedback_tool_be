const express = require("express");
const submitFormController = require("../controllers/submitFormController");

const router = express.Router();

router.post("/", submitFormController.createSubmitForm);
router.get("/", submitFormController.getAllSubmitForm);
router.get("/:id", submitFormController.getSubmitFormById);

module.exports = router
