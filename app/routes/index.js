const userRouter = require('./userRouter');
const formRouter = require('./formRouter');
const submitFormRouter = require('./submitFormRouter');
const widgetRouter = require('./widgetRouter');
const thanksMsgRouter = require('./thanksMsgRouter');
const feedbackRouter = require('./feedback');

function routes (app) {
    app.use('/feedback', feedbackRouter)
    app.use('/feedback/users', userRouter)
    app.use('/feedback/forms', formRouter)
    app.use('/feedback/submit', submitFormRouter)
    app.use('/feedback/widget', widgetRouter)
    app.use('/feedback/thanks', thanksMsgRouter)
}

module.exports = routes