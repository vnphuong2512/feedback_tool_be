const express = require("express");
const thanksController = require("../controllers/thanksController");

const router = express.Router();

// router.post("/users", addUser);
// router.get("/users", getAllUsers);
// router.get("/users/:id", getUserById);

router.post("/", thanksController.createThanksContent);
router.get("/", thanksController.getAllThankData);
router.get("/:id", thanksController.getThanksDataById);

module.exports = router
