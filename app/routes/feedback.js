const feedbackController = require('../controllers/feedbackController');
const router = require('express').Router()

router.get('/', feedbackController.renderUI)
router.get('/file', feedbackController.renderScriptFile)

module.exports = router