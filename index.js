const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const upload = require('express-fileupload')
const routes = require('./app/routes')
const { engine } = require('express-handlebars')
const path = require('path')

require("dotenv").config()

const app = express();
app.use(express.json({ limit: "10mb", extended: true }));
app.use(
  express.urlencoded({ limit: "10mb", extended: true, parameterLimit: 60000 })
);

app.use(express.static(path.join(__dirname, 'uploads')))
app.use(express.static(path.join(__dirname, 'submission')))
app.use(express.json({ encoding: "utf8" })); // Use utf8 encoding for JSON parsing
app.use(express.urlencoded({ extended: true, encoding: "utf8" }));
app.use(upload());
app.use(cors());
app.use(bodyParser.json());
app.engine('hbs', engine({ extname: '.hbs'}))
app.set('view engine', 'hbs');

app.set('views', path.join(__dirname, 'app', 'views'));
// app.use("/", userRoutes.routes);
// app.use("/", formRoutes.routes);
// app.use("/", submitFormRoutes.routes);

app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader(
    "content-type", "text/html"
  );
  
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});
app.get('/', (req,res) =>{
  res.send('Hello world')
})

routes(app)
const PORT = process.env.PORT
app.listen(PORT, () =>
  console.log("APP is running on port: " + PORT)
);
